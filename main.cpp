#include <stdio.h>
#include <windows.h>
#include "string_lib.h"

int main()
{
    SetConsoleOutputCP( CP_UTF8 );
    const int n = 18;
    char str[n];

    printf("Введите строку...\n" );
    if (FGetS( str, n, stdin ) ) {
        printf("Проверка работы функций FGetS и FPutS...\n" );
        FPutS( str, stdout );
    }
    char check[] = "-12345";
    printf("AToI на строке %s работает так: %d\n", "123a", AToI( check ) );
    IToA( AToI( check ), str, 10);
    printf("ItoA на числе %d работает так: ", AToI( check ) );
    FPutS( str, stdout );
    int ch = 1;
    printf( "int ch = %d\n", ch );
    swapBytesOfInt_p( &ch );
    printf( "Число с переставленными байтами (с указателями) (ch) = %d\n", ch );
    swapBytesOfInt_m( &ch );
    printf( "Число с переставленными байтами (с математикой) (ch) = %d\n", ch );
    return 0;
}







