//
// Created by dimoha_zadira on 05.09.2019.
//

#include <assert.h>
#include <stdio.h>
#include <math.h>

void swapBytesOfInt_p( const int *p )
{
    char tmp;
    char *c = (char*) p;
    tmp = *c;
    *c = *( c + 3 );
    *( c + 3 ) = tmp;
    tmp = *( c + 1 );
    *( c + 1 ) = *( c + 2 );
    *( c + 2 ) = tmp;

}

void swapBytesOfInt_m( int *p )
{
    char *c = (char*) p;
    int m1, m2, m3, m4;

    m1 = (int)*c;
    m2 = (int)*( c + 1 );
    m3 = (int)*( c + 2 );
    m4 = (int)*( c + 3 );
    *p = (int) (pow(256,3) * m1 + pow(256, 2) * m2 + 256 * m3 + m4);

}

void fillStr( char *str, int size, const int radix, int n )
{
    for ( size; size > 0; size-- ){
        if ( ( n % radix >= 0 ) && ( n % radix < 10 ) ) *str = (n % radix) + '0'; //если надо поставить ЦИФРУ
        else *str = n % radix + 'A' - 10;//если надо поставить БУКВУ
        n /= radix;
        str--;
    }
}

void countSize( int n, const int radix, int *size ) //подсчитываем количество символов числа n в нужной системе счисления
{
    for( n; n > 0; n /= radix  ){
        (*size)++;
    }
}

char *IToA( int n, char *str, const int radix )
{
    assert ( str != nullptr );
    assert ( ( radix > 1 ) && ( radix < 37 ) );
    if ( n < 0 ){
        *str = '-';
        str++;
        n *= -1;
    }
    int size = 0;
    countSize(n, radix, &size); //подсчитываем количество символов числа n в нужной системе счисления
    str += size; //откатились в конец, чтобы записывать переведённое в новую СС число С КОНЦА
    *str = '\0'; //здесь будет конец строки
    str--; //чтобы корректно записывать
    fillStr(str, size, radix, n);//заполняем
    return str;
}

char* FPutS( char *str, FILE *out )
{
    assert ( ( str != nullptr ) && ( out != nullptr ) );
    for (int i = 0; str[i] != '\0'; i++ ) fputc( str[i], out ); // пока не конец файла, кладём в out текущий char
    fputc( '\n', out);
    return str;
}

char *FGetS( char *str, int n, FILE *in )
{
    assert( str != nullptr );
    assert( in != nullptr );
    char buf; //буфер, куда будем считывать
    char *p = str;//нужно, чтобы в конце вернуть str
    while( n > 1 ){ //пока есть куда записывать
        n--;
        buf = fgetc ( in ); //читаем буфер
        if( buf == EOF ) break; //файл кончился, пора спатки
        *p = buf; //пишем в строку
        p++; //двигаем указатель строки на следующий символ
        if ( buf == '\n' ) break; //строчка кончилась, пора спатки
    }
    *p = '\0'; //заканчиваем строку
    return str;
}

int AToI( const char *str )
{
    assert( str != nullptr );
    int ans = 0;
    while ( *str ) {
        if( ( *str > '9' ) || ( *str < '0' ) ){
            if ( *str != ' ' ) break; //если текущий символ -- не цифра (кроме пробела)
        }
        if( ( *str > '9' ) || ( *str < '0' ) ) break; //если текущий символ -- не цифра (кроме пробела)
        ans = ans * 10 + *str - '0'; // вычитание '0', чтобы привести номер в ascii к int
        str++;
    }
    return ans;
}

