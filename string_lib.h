//
// Created by dimoha_zadira on 05.09.2019.
//

#ifndef LION_PROJECTS_STRING_LIB_H
#define LION_PROJECTS_STRING_LIB_H

char* FGetS( char *str, int n, FILE *in );
char* FPutS( char *str, FILE *out );
int AToI( const char *str );
char *IToA(int num, char *str, const int radix);
void countSize( int n, int radix, int *size );
void fillStr( char *str, int size, const int radix, int n );
void swapBytesOfInt_p( const int *p );
void swapBytesOfInt_m( int *p );

#endif //LION_PROJECTS_STRING_LIB_H
